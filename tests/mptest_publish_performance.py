import time

import logging
import mqttmgr

PUBLISH_CNT = 10_000

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.name}:{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)

mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                           "user",
                           "passwd",
                           client_id=f'test_mqtt_client',
                           )
mqtt_mgr.connect()

print(f"Started client performance test. Sending {PUBLISH_CNT} messages")
start = time.time()
for i in range(PUBLISH_CNT):
    mqtt_mgr.publish("test_topic", f"{i}", qos=0)
duration = time.time() - start
mqtt_mgr.publish("test_topic", f"exit")

print(f"Send {PUBLISH_CNT} messages in {duration}s. ({duration/PUBLISH_CNT*1000} ms/msg)")