import asyncio
import time

import aiomqtt

async def test_body():
    i = 0
    start_time = None
    disconnect_time = None
    restart_time = None
    fail_time = None
    print("Started status check")
    async with aiomqtt.Client("127.0.0.1",
                              1883,
                              timeout=10,
                              ) as client:
        await client.subscribe("/test/test_dev/status")
        async for msg in client.messages:
            print(f"Received mqtt {msg.payload.decode()}")
            if i == 0:
                assert msg.payload.decode() == "ONLINE"
                start_time = time.time()
            if i == 1:
                assert msg.payload.decode() == "OFFLINE"
                disconnect_time = time.time()
            if i == 2:
                assert msg.payload.decode() == "ONLINE"
                restart_time = time.time()
            if i == 3:
                assert msg.payload.decode() == "FAILED"
                fail_time = time.time()
            if i == 3: # Get stats after last expected message
                disc_time = abs((disconnect_time - start_time) - 5.0)
                recon_time = abs((restart_time - disconnect_time) - 5.0)
                fail_time = abs((fail_time - restart_time) - 5.0)
                print(f"Disconnect time: {disc_time}s reconnect time: {recon_time} fail time: {fail_time}")
                assert disc_time < 0.5
                assert recon_time < 0.5
                assert fail_time < 0.5
                exit(0)
            i = i + 1


asyncio.run(test_body())