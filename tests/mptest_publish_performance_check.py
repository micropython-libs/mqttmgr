import asyncio
import time

import aiomqtt

async def test_body():
    i = 0
    errors = 0
    start = None
    print("Started performance check")
    async with aiomqtt.Client("127.0.0.1",
                              1883,
                              timeout=10,
                              ) as client:
        await client.subscribe("test_topic")
        async for msg in client.messages:
            if start is None:
                start = time.time()
            if msg.payload == b"exit":
                print("Received exit")
                break
            # assert msg.payload == f"{i}".encode("utf8")
            exp_msg = f"{i}".encode("utf8")
            if msg.payload != exp_msg:
                print(f"Error expected/received message does not match {msg.payload}!={exp_msg}")
                errors += 1
            i = int(msg.payload.decode("utf8")) + 1
    duration = time.time() - start
    print(f"Received {i} ({errors} errors) messages in {duration}s ({duration/i*1000})ms/msg")

asyncio.run(test_body())