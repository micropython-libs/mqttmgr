import time
import asyncio

import logging
import mqttmgr

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.name}:{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)

req_exit = asyncio.Event()

last_msg = None
received_cnt = 0
errors_cnt = 0
start = None
def check_msg(topic, data):
    global last_msg
    global start
    global received_cnt
    received_cnt += 1
    if start is None:
        print("Start timer")
        start = time.time()
    if data == b"exit":
        print("Received exit")
        req_exit.set()
        return
    if last_msg is not None:
        rec_data = int(data.decode("utf8"))
        last_msg_int = int(last_msg.decode("utf8"))
        if last_msg_int+1 != rec_data:
            print(f"Received wong data {last_msg_int+1} != {rec_data}")
        last_msg = data

async def do_test():
    mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                               "user",
                               "passwd",
                               client_id=f'test_mqtt_client',
                               msg_sleep=1,
                               )
    mqtt_mgr.connect()
    mqtt_mgr.add_callback("test_topic", check_msg)

    print(f"Started subscribe performance test.")
    await req_exit.wait()
    duration = time.time() - start
    print(f"Received {received_cnt} messages in {duration}s. ({duration / received_cnt * 1000} ms/msg)")

asyncio.run(do_test())

