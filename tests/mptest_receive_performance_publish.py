import asyncio
import time

import aiomqtt

MSG_CNT = 10_000
async def test_body():
    i = 0
    errors = 0
    start = time.time()
    print("Started performance check")
    async with aiomqtt.Client("127.0.0.1",
                              1883,
                              timeout=10,
                              ) as client:
        for i in range(MSG_CNT):
            await client.publish("test_topic", f"{i}")
        await client.publish("test_topic", "exit")

    duration = time.time() - start
    print(f"Published {MSG_CNT} ({errors} errors) messages in {duration}s ({duration/i*1000})ms/msg")


asyncio.run(test_body())