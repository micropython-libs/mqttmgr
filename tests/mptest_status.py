import time

import logging
import mqttmgr

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.ct}:{record.name}:{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)

mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                           "user",
                           "passwd",
                           client_id=f'test_mqtt_client',
                           status_topic="/test/test_dev/status",
                           last_will_msg="FAILED",
                           disconnect_msg="OFFLINE",
                           online_msg="ONLINE",
                           )

logger.info("Connecting")
mqtt_mgr.connect()
logger.info("Connected")
time.sleep(5)
logger.info("Disconnecting")
mqtt_mgr.disconnect()
logger.info("Disconnected")
time.sleep(5)
logger.info("Connecting second time")
mqtt_mgr.connect()
logger.info("Connected")
time.sleep(5)
# Just die -> fail connection
logger.info("Finishing")