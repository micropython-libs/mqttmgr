import usocket as socket
import ustruct as struct
from ubinascii import hexlify
import errno
try:
    import uasyncio as asyncio
except ImportError:
    import asyncio
import time

loop = asyncio.get_event_loop()

class MQTTException(Exception):
    pass

class MQTTClient:
    def __init__(
        self,
        client_id,
        server,
        port=0,
        user=None,
        password=None,
        keepalive=0,
        ssl=False,
        ssl_params={},
    ):
        if port == 0:
            port = 8883 if ssl else 1883
        self.client_id = client_id
        self.sock = None
        self.server = server
        self.port = port
        self.ssl = ssl
        self.ssl_params = ssl_params
        self.pid = 0
        self.cb = None
        self.user = user
        self.pswd = password
        self.keepalive = keepalive
        self.lw_topic = None
        self.lw_msg = None
        self.lw_qos = 0
        self.lw_retain = False

    def _send_str(self, s):
        self.sock.write(struct.pack("!H", len(s)))
        self.sock.write(s)

    def _recv_len(self):
        n = 0
        sh = 0
        while 1:
            b = self.sock.read(1)[0]
            n |= (b & 0x7F) << sh
            if not b & 0x80:
                return n
            sh += 7

    def set_callback(self, f):
        self.cb = f

    def set_last_will(self, topic, msg, retain=False, qos=0):
        assert 0 <= qos <= 2
        assert topic
        self.lw_topic = topic
        self.lw_msg = msg
        self.lw_qos = qos
        self.lw_retain = retain

    def connect(self, clean_session=True):
        self.sock = socket.socket()
        addr = socket.getaddrinfo(self.server, self.port)[0][-1]
        self.sock.connect(addr)
        if self.ssl:
            import ussl

            self.sock = ussl.wrap_socket(self.sock, **self.ssl_params)
        premsg = bytearray(b"\x10\0\0\0\0\0")
        msg = bytearray(b"\x04MQTT\x04\x02\0\0")

        sz = 10 + 2 + len(self.client_id)
        msg[6] = clean_session << 1
        if self.user is not None:
            sz += 2 + len(self.user) + 2 + len(self.pswd)
            msg[6] |= 0xC0
        if self.keepalive:
            assert self.keepalive < 65536
            msg[7] |= self.keepalive >> 8
            msg[8] |= self.keepalive & 0x00FF
        if self.lw_topic:
            sz += 2 + len(self.lw_topic) + 2 + len(self.lw_msg)
            msg[6] |= 0x4 | (self.lw_qos & 0x1) << 3 | (self.lw_qos & 0x2) << 3
            msg[6] |= self.lw_retain << 5

        i = 1
        while sz > 0x7F:
            premsg[i] = (sz & 0x7F) | 0x80
            sz >>= 7
            i += 1
        premsg[i] = sz

        self.sock.write(premsg, i + 2)
        self.sock.write(msg)
        # print(hex(len(msg)), hexlify(msg, ":"))
        self._send_str(self.client_id)
        if self.lw_topic:
            self._send_str(self.lw_topic)
            self._send_str(self.lw_msg)
        if self.user is not None:
            self._send_str(self.user)
            self._send_str(self.pswd)
        resp = self.sock.read(4)
        assert resp[0] == 0x20 and resp[1] == 0x02
        if resp[3] != 0:
            raise MQTTException(resp[3])
        return resp[2] & 1

    def disconnect(self):
        self.sock.write(b"\xe0\0")
        self.sock.close()

    def ping(self):
        self.sock.write(b"\xc0\0")

    def publish(self, topic, msg, retain=False, qos=0):
        to_send = b""
        _s = time.ticks_ms()
        pkt = bytearray(b"\x30\0\0\0")
        pkt[0] |= qos << 1 | retain
        sz = 2 + len(topic) + len(msg)
        if qos > 0:
            sz += 2
        assert sz < 2097152
        i = 1
        while sz > 0x7F:
            pkt[i] = (sz & 0x7F) | 0x80
            sz >>= 7
            i += 1
        pkt[i] = sz
        # print(hex(len(pkt)), hexlify(pkt, ":"))
        #self.sock.write(pkt, i + 1)
        i = i + 1
        to_send += pkt[0: i]
        ###print(f"MQTT W1 in {time.ticks_ms()-_s}ms")
        #self._send_str(topic)
        to_send += struct.pack("!H", len(topic))
        to_send += topic
        ###print(f"MQTT W2 in {time.ticks_ms()-_s}ms")
        if qos > 0:
            self.pid += 1
            pid = self.pid
            struct.pack_into("!H", pkt, 0, pid)
            #self.sock.write(pkt, 2)
            to_send += pkt[0:2]
        ###print(f"MQTT W3 in {time.ticks_ms()-_s}ms")
        start_time = time.ticks_ms()
        self.sock.write(to_send)
        self.sock.write(msg)
        end_time = time.ticks_ms()
        ###print(f"MQTT WRITE {len(msg)//1024}kb in {end_time-start_time}ms")
        if qos == 1:
            while 1:
                op = self.wait_msg()
                if op == 0x40:
                    sz = self.sock.read(1)
                    assert sz == b"\x02"
                    rcv_pid = self.sock.read(2)
                    rcv_pid = rcv_pid[0] << 8 | rcv_pid[1]
                    if pid == rcv_pid:
                        return
        elif qos == 2:
            assert 0

    def subscribe(self, topic, qos=0):
        print(f"SUBSCRIBING: {topic}")
        # TODO: Fix this
        assert self.cb is not None, "Subscribe callback is not set"
        pkt = bytearray(b"\x82\0\0\0")
        self.pid += 1
        struct.pack_into("!BH", pkt, 1, 2 + 2 + len(topic) + 1, self.pid)
        # print(hex(len(pkt)), hexlify(pkt, ":"))
        self.sock.write(pkt)
        self._send_str(topic)
        self.sock.write(qos.to_bytes(1, "little"))
        while 1:
            op = self.wait_msg()
            if op == 0x90:
                resp = self.sock.read(4)
                assert resp[1] == pkt[2] and resp[2] == pkt[3]
                if resp[3] == 0x80:
                    raise MQTTException(resp[3])
                return

    def unsubscribe(self, topic):
        pass # TODO: Implement this

    # Wait for a single incoming MQTT message and process it.
    # Subscribed messages are delivered to a callback previously
    # set by .set_callback() method. Other (internal) MQTT
    # messages processed internally.
    def wait_msg(self, ignore_missing=False):
        try:
            res = self.sock.read(1)
        except OSError as e:
            if ignore_missing and e.errno:
                print(f"wait_msg raised OS error - but we expected this might happen {e.errno}")
                return None
            else:
                raise e
        self.sock.setblocking(True)
        if res is None:
            return None
        if res == b"":
            raise OSError(-1)
        if res == b"\xd0":  # PINGRESP
            sz = self.sock.read(1)[0]
            assert sz == 0
            return None
        op = res[0]
        if op & 0xF0 != 0x30:
            return op
        sz = self._recv_len()
        topic_len = self.sock.read(2)
        topic_len = (topic_len[0] << 8) | topic_len[1]
        topic = self.sock.read(topic_len)
        sz -= topic_len + 2
        if op & 6:
            pid = self.sock.read(2)
            pid = pid[0] << 8 | pid[1]
            sz -= 2
        msg = self.sock.read(sz)
        self.cb(topic, msg)
        if op & 6 == 2:
            pkt = bytearray(b"\x40\x02\0\0")
            struct.pack_into("!H", pkt, 2, pid)
            self.sock.write(pkt)
        elif op & 6 == 4:
            assert 0
        return op

    # Checks whether a pending message from server is available.
    # If not, returns immediately with None. Otherwise, does
    # the same processing as wait_msg.
    def check_msg(self):
        self.sock.setblocking(False)
        return self.wait_msg(ignore_missing=True)

class MqttMgr():
    def __init__(self, hostname, user="user", password="password",
                 client_id="umqtt_client", auto_reconnect=True, mqtt_prefix=None,
                 msg_sleep=100, reconnect_timeout=1_000, status_topic=None, last_will_msg=None, disconnect_msg=None,
                 online_msg=None):
        """

        Args:
            hostname:
            user:
            password:
            client_id:
            auto_reconnect:
            mqtt_prefix:
            msg_sleep: sleep between messages in ms
        """
        self.hostname = hostname
        if mqtt_prefix is not None:
            mqtt_prefix = self._str_to_bytes(mqtt_prefix)
            self.mqtt_prefix = mqtt_prefix.strip(b"/") + b"/"
        else:
            self.mqtt_prefix = b""
        self.msg_sleep = msg_sleep
        self.reconnect_timeout = reconnect_timeout
        self.status_topic = self._str_to_bytes(status_topic) if status_topic is not None else None
        self.last_will_msg = self._str_to_bytes(last_will_msg) if last_will_msg is not None else None
        self.online_msg = self._str_to_bytes(online_msg) if online_msg is not None else None
        self.disconnect_msg = self._str_to_bytes(disconnect_msg) if disconnect_msg is not None else None
        self.mqtt = MQTTClient(client_id, self.hostname, port=1883,
                               user=user, password=password,
                               keepalive=10_000, ssl=False)
        if self.status_topic and self.last_will_msg:
            self.mqtt.set_last_will(self.status_topic, self.last_will_msg, True, 1)
        self.auto_reconnect = auto_reconnect
        self.check_task = None
        self.mqtt_online = False
        self.callbacks = []

    def _str_to_bytes(self, value):
        if isinstance(value, (bytes, bytearray)):
            return value
        else:
            return value.encode('utf8')

    def _receive_mqtt(self, topic, msg):
        for rec_topic, rec_callback in self.callbacks:
            try:
                if rec_topic==topic: # TODO: Do full topic check (wild characters)
                    rec_callback(topic, msg)
            except Exception as e:
                print(
                    f"Callback {rec_callback} raised {e}({type(e)}) on {topic}:{msg}")

    def add_callback(self, topic, callback):
        topic = self._str_to_bytes(topic)
        topic.strip(b"/")
        topic = self.mqtt_prefix + topic #TODO: mqtt_prefix should not be used here
        record = (topic, callback)
        if record in self.callbacks:
            pass # TODO: Print warning
        self.callbacks.append(record)
        try:
            self.mqtt.subscribe(topic)
        except Exception as e:
            print(f"Exception {e} ({type(e)}) when subscribing in runtime")
            self.mqtt_online = False

    def del_callback(self, topic, callback):
        topic = self._str_to_bytes(topic)
        topic.strip(b"/")
        topic = self.mqtt_prefix + topic  # TODO: mqtt_prefix should not be used here
        record = (topic, callback)
        if record not in self.callbacks:
            pass  # TODO: Print error
            return
        self.callbacks.remove(record)
        # Unsubscribe if there is no other subscription for this topic
        for rec in self.callbacks:
            if rec[0] == topic:
                break
        else:
            self.mqtt.unsubscribe(topic)

    def connect(self):
        self.mqtt.set_callback(self._receive_mqtt)
        try:
            self.mqtt.connect()
            for rec_topic, rec_callback in self.callbacks:
                self.mqtt.subscribe(rec_topic)
            if self.status_topic and self.disconnect_msg:
                self.mqtt.publish(self.status_topic, self.online_msg, True, 1)
            self.mqtt_online = True
        except Exception as e:
            print(f"Mqtt connection failed ({e}, {type(e)})")
            self.mqtt_online = False
        if self.check_task is None:
            self.check_task = loop.create_task(self.check_msg())

    # TODO: Disconnect
    def disconnect(self):
        if self.status_topic and self.disconnect_msg:
            self.mqtt.publish(self.status_topic, self.disconnect_msg, True, 1)
        self.mqtt.disconnect()

    def check_msg(self):
        print("Starting check subroutine")
        while True:
            if self.auto_reconnect and not self.mqtt_online:
                print("Reconnecting")
                yield from asyncio.sleep_ms(self.reconnect_timeout)
                ret = self.connect()
            try:
                start_time = time.ticks_ms()
                self.mqtt.check_msg()
                end_time = time.ticks_ms()
                if end_time - start_time > 10:
                    print(f"check_msg update took {end_time-start_time}ms")
            except OSError as e:
                err_str = errno.errorcode.get(e.errno, "Unknown errno")
                print(f"OSError exception {e} ({type(e)}) errno:{e.errno} - {err_str}")
                self.mqtt_online = False
            except Exception as e:
                print(f"Mqtt check_msg failed with:{e} ({type(e)})")
                self.mqtt_online = False
            yield from asyncio.sleep_ms(self.msg_sleep)

    def publish(self, topic, msg, retain=False, qos=0):
        topic = self._str_to_bytes(topic)
        msg = self._str_to_bytes(msg)
        topic.strip(b"/")
        topic = self.mqtt_prefix + topic
        try:
            self.mqtt.publish(topic, msg, retain, qos)
        except OSError as e:
            print(f"Mqtt publish failed with:{e} ({type(e)})")
            self.mqtt_online = False
